# POSSIBLE OPEN DATA RESOURCES

These are a few of data resources for neuroscience.

Most of them are open, some of them may require account or permissions.

[[_TOC_]]

## Neuroscience - _Specific modules_

These have data for each specific module.

### Connectivity

- Brain Observatory Storage Service & Database (EM): https://bossdb.org/projects
- FlyLight: https://www.janelia.org/project-team/flylight
- Virtual Fly Brain (connectivity): https://www.virtualflybrain.org/
- Neuprint: https://neuprint.janelia.org/
- Flywire: https://flywire.ai/
- MouseLight: https://ml-neuronbrowser.janelia.org/

### Identity

#### Sequencing

- Brain Cell Data (single-cell, spatial): https://www.braincelldata.org/
  - Has web interface
  - Data can be read with anndata, scanpy, seurat, ...
- Neurosci Multi-omic Archive: https://nemoarchive.org/
- SCope (scRNA-seq): https://scope.aertslab.org/
  - Has web interface
  - Data can be read with anndata, scanpy, loompy, ...
- CellXGene: https://cellxgene.cziscience.com/datasets
  - Nice web interface
- CellBrowser UCSC: https://cells.ucsc.edu
  - Has web interface
  - Data can be read with scanpy/anndata/suerat: https://cellbrowser.readthedocs.io/en/master/load.html#scanpy
- KaessmannLab: https://home.kaessmannlab.org/resources
  - e.g. evolution & devlopment of cerrebellum: https://apps.kaessmannlab.org/sc-cerebellum-transcriptome/

#### Beyond sequencing

- NeuroElectro: https://neuroelectro.org
- NeuroMorpho: https://neuromorpho.org
- Allen Brain Multi-modal: https://portal.brain-map.org/explore/classes/multimodal-characterization, eg. [V1 mouse interneuron patch-seq](https://knowledge.brain-map.org/data/1HEYEW7GMUKWIQW37BO/summary)

### Activity

- Dandi Archive (neurophysiology): https://dandiarchive.org/.
  - Note: also has data from Allen + MICrONS, IBL below
  - Has web interface (neurosift) or use nwbwidgets for interaction on notebook
  - Data can be accessed and streamed with NWB
  - Provides DandiHub for exploratory
  - semantic search with LLM: https://llmsearch.dandiarchive.org/
- IBL: https://www.internationalbrainlab.com/data
  - Has web interface
  - Data can be read with IBL ONE, or via NWB
- CRCNS: https://crcns.org/data-sets/
  - Need an account to download data
- buzsakilab databank: https://buzsakilab.com/wp/database/
  - datasets: https://buzsakilab.nyumc.org/datasets/
  - Data can be accessed via Globus
- neuroelectro (electrophysiology, identity/activity): https://neuroelectro.org/

### Other

- GIN G-Node (physiology, behaviors, simulations, ...): https://doi.gin.g-node.org/
- Brain Image Library: https://submit.brainimagelibrary.org/search
- neuromorpho (morphology data, identity): https://neuromorpho.org/

## Neuroscience - _Across modules_

These have data for more than 1 modules

### Allen Brain

Allen Brain (non-exhaustive): https://portal.brain-map.org/

- Data can be read with allensdk / NWB:
  - see their documentation: https://allensdk.readthedocs.io/en/latest/
  - see https://nwb4edu.github.io/ for some tutorials
- Data catalog (search engine for different archives): https://knowledge.brain-map.org/data
  - some datasets come with very nice user interfaces
- Allen Brain Connectivity Atlas: https://connectivity.brain-map.org/
- Synaptic physiology (conn): https://portal.brain-map.org/explore/connectivity/synaptic-physiology
- RNA sequencing: https://portal.brain-map.org/atlases-and-data/rnaseq
- Allen Brain Cell Atlas ABC Atlas (sequencing): https://knowledge.brain-map.org/data/LVDBJAW8BI5YSS1QUBG/explore
- Observatory (2P activity): https://observatory.brain-map.org/visualcoding/

### MICrONS

Link: https://www.microns-explorer.org/

- Phase 1 EM mouse V1 L2/3 (EM connectivity): https://www.microns-explorer.org/phase1
- Cortical MM3 (EM, synaptic connectivity, 2P activity, ...): https://www.microns-explorer.org/cortical-mm3
- Data access:
  - Calcium imaging can be accessed via Dandi/NWB: https://dandiarchive.org/dandiset/000402
  - Some data seem to also be on Zenodo:
  - https://zenodo.org/records/8364070
  - https://zenodo.org/records/7510511
  - General access with their API (need to use your google account for access): https://github.com/cajal/microns_phase3_nda
- Co-registration EM/imaging data can be accessed via (possibly no need for Dandi/NWB): https://github.com/cajal/microns_phase3_nda/blob/v8/tutorial_notebooks/Matched_Cell_Functional_Data.ipynb

## Disease specific

Disease related datasets are usually sequencing datasets, in which the above resources usually have them (e.g. UCSC Cell Browser, Allen data portal, CellXGene, ...), below are some other resources. See the activity slides for some notes on finding disease-related datasets as well.

### Alzheimer's disease

- Model AD: https://sagebio.shinyapps.io/MODEL_AD_Explorer/ (also press release)
- Synapse: https://www.synapse.org/#!Synapse:syn2580853/datasets/
  - Synapse seems to have quite many disease-related datasets but you need an account
  - And some datasets may be still limited in use or access

### Allen

Allen has many resources, mostly sequencing or ISH data, e.g.

- AD: 
  - https://portal.brain-map.org/explore/seattle-alzheimers-disease
  - Alzheimer spatial transcriptomics: https://knowledge.brain-map.org/data/FHUUK19A5XJ7FC9YYT4/explore?filterOptions%5Ba%5D=&genes=a%3A&layoutState=Single&views=a~19546.585-19546.585_69229.7973982831-41047.8285_0_web-image&visualizations=a~QNZMGOZXH4EPCIU3V3A%3AY1EM7CBU8LJUK815ABL~METADATA%3ABZY7XBG34URAT1IU3C7%3Anull%3Anull%3Anull%3Afalse%3A0.5 
- aging: https://aging.brain-map.org/
- glioblastoma: https://glioblastoma.alleninstitute.org/
- schizophrenia: https://human.brain-map.org/ish/search/?page_num=0&page_size=400&no_paging=false&search_term=&search_type=schizophrenia
- ASD, sleep, ... (see https://portal.brain-map.org/, click on "Atlases and Data")

## Human electrophys

- OpenNeuro: https://openneuro.org/
- GIN G-Node, see this lab for example iEEG datasets with spikes: https://gin.g-node.org/USZ_NCH
  Dandi archive, e.g. if want spikes search for "homo sapiens spikes" + read the metadata (e.g. "spike sorting" should be there) and use neurosift to explore

## Generalist

Use this to find data if the above are not sufficient.

Sometimes the data are deposited on here during/after publications.

- Zenodo: https://zenodo.org/
- Figshare: figshare.com
- Dryad: https://datadryad.org/

Repository of Repositories (use this to find more data archives):

- Registry of Research Repository: https://www.re3data.org/
- AWS Open Data: https://aws.amazon.com/opendata
