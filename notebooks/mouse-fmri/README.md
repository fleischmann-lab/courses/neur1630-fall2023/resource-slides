# Demo of obtaining functional connectivity using `nilearn` of mouse datasets

This demonstrates how to use `nilearn` and the Allen CCF to obtain time series and functional connectivity from fMRI datasets from mouse datasets.

Note: I (Tuan) do not know anything about fMRI; please use with caution.

## Data

See `data/README.md` for more details of the datasets and how to download them. 

## Requirements

If you are on a terminal, inside the relevant environment, run:

```bash
pip install -r requirements.txt
```

If you are in a notebook on JupyterHub or Colab, run this in the top of the notebook:

```python
%pip install -r requirements.txt
```

**A big note** on `antspyx` installation, if you do `pip install antspyx`, you might have to wait a very long time. Based on their [documentation](https://github.com/ANTsX/ANTsPy/blob/master/tutorials/InstallingANTsPy.md), you can use the published wheel but you'd have to find the actual link yourself, for example if my environment is `python 3.11` and my system is Linux then do this:

```
pip install https://github.com/ANTsX/ANTsPy/releases/download/v0.4.2/antspyx-0.4.2-cp311-cp311-manylinux_2_17_x86_64.manylinux2014_x86_64.whl
```

