# Data

This file contains the instructions to download the data necessary.

## Allen CCF

The folder `allen-ccf` contains Allen Mouse Brain Common Coordinate Framework atlas data.

> Wang, Quanxin, et al. “The Allen Mouse Brain Common Coordinate Framework: A 3D Reference Atlas.” Cell, vol. 181, no. 4, May 2020, pp. 936-953.e20. DOI.org (Crossref), https://doi.org/10.1016/j.cell.2020.04.007.

To download, if not already available, go to `allen-ccf/sources.txt` and download each file separately into that folder. These files are expected:

```
annotation_100.nii.gz
ara_nissl_100.nii.gz
average_template_100.nii.gz
itksnap_label_description.txt
```

When you read the `sources.txt` files and visit those links, you could replace `100` with `50`, `25` or `10`. But note that the lower you go, the bigger the files (higher resolution), and may slow down computation.

## OpenNeuro datasets

The following datasets are from this paper:

> Mandino F, Yeow LY, Bi R, Sejin L, Bae HG, Baek SH, Lee CY, Mohammad H, Horien C, Teoh CL, Lee JH, Lai MK, Jung S, Fu Y, Olivo M, Gigg J, Grandjean J. The lateral entorhinal cortex is a hub for local and global dysfunction in early Alzheimer's disease states. J Cereb Blood Flow Metab. 2022 Sep;42(9):1616-1631. doi: 10.1177/0271678X221082016. Epub 2022 Apr 25. PMID: 35466772; PMCID: PMC9441719.

### resting fMRI

The `ds001890` folder contains resting fMRI mouse data from <https://openneuro.org/datasets/ds001890/versions/1.0.1>. 

> Francesca Mandino and Yeow Ling Yun and John Gigg and Malini C. Olivo and Joanes Grandjean (2019). Mouse_rest_3xTG. OpenNeuro. [Dataset] doi: 10.18112/openneuro.ds001890.v1.0.1

To download each file separately, you'd have to manually click on the download buttons for the relevant files.

To download the whole thing programmatically, head to their `Download` tab to see the different options.

- If you don't want to install extra things, go to `Download` tab, click on `Download shell script`. you just need to put this script inside `ds001890` (create this folder if it doesn't exist) and run it in the terminal.
- You could also look into `datalad` (go to <https://www.datalad.org/#install> to know how to install it), then follow their instruction to download it.

### opto fMRI

The `ds002134` folder contains resting fMRI mouse data from <https://openneuro.org/datasets/ds002134/versions/1.0.0>. 

> Francesca Mandino and Yeow Ling Yun and John Gigg and Malini C. Olivo and Joanes Grandjean (2019). Mouse_opto_3xTG. OpenNeuro. [Dataset] doi: 10.18112/openneuro.ds002134.v1.0.0

To download each file separately, you'd have to manually click on the download buttons for the relevant files.

To download the whole thing programmatically, head to their `Download` tab to see the different options.

- If you don't want to install extra things, go to `Download` tab, click on `Download shell script`. you just need to put this script inside `ds002134` (create this folder if it doesn't exist) and run it in the terminal.
- You could also look into `datalad` (go to <https://www.datalad.org/#install> to know how to install it), then follow their instruction to download it.
