# Image dimensions

These are helper notebooks created for the projects related to looking at neural activity and the natural scene stimuli from the Allen Brain experiments.

These notebooks quantify the different metrics for an image.

> Q: I cannot view some notebooks on Gitlab?

I do not know why and haven't had the time to figure it out, but you should be able download the notebook (or clone the whole repository).

Alternatively, for quick viewing without any downoad, you can use <https://nbviewer.org/>

For example, to view `image-resnet-features.ipynb`, visit this

```
https://gitlab.com/fleischmann-lab/courses/neur1630-fall2023/general-resources/-/raw/main/notebooks/images/image-resnet-features.ipynb
```

This notebook in particular has an interactive plot, which is the only part that does not render on Gitlab, but does on nbviewer.

Some notebooks that are known to not render on Gitlab are:

- `image-zeroshot-classify.ipynb`, so visit: <https://nbviewer.org/urls/gitlab.com/fleischmann-lab/courses/neur1630-fall2023/general-resources/-/raw/main/notebooks/images/image-zeroshot-classify.ipynb>
- `image-fractal-dims.ipynb`, so visit: <https://nbviewer.org/urls/gitlab.com/fleischmann-lab/courses/neur1630-fall2023/general-resources/-/raw/main/notebooks/images/image-fractal-dims.ipynb>


