# Helper notebooks regarding metadata querying

These notebooks are from others in the course that can be used for querying metadata from open data resources.

- [`QueryIBLONEByTimeFrameUsed.ipynb`] by Evan F

> This is some code that searches through IBL to find mice that were used multiple times. For example, one mouse might have been used on January 3rd, while the same mouse was used again in the same experiment on January 5th. This could be useful for someone looking into aging, memory reinforcement, adaptation, etc.
