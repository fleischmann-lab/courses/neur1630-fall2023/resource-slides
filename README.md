# NEUR1630 General Resources

This repository contains a subset of general resources for the NEUR1630 (Fall 2023) course on Big Data Neuroscience Ideas Lab.

## Data

The file `data-resources.md` contains a list of (suggested) potential data resources to find data to analyze.

## Slides

Some of the slides for demos of computational resources are in the `slides` folder.

## Notebooks

The folder `notebooks` contain some miscellaneous codes that are not in the demos, or can be applied in other modules.

Many of these also came out of requests or questions from others in the class.

Note: Some of these were run on the course JupyterHub, some on Google Colab, one on Oscar.

